﻿using Amazon.SQS;
using Amazon.SQS.Model;
using System.Collections.Generic;
using System.Threading;
using log4net;
using System.Reflection;

namespace Tooling.SQS
{
    public class SQSTooling
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private string _queueUrl;
        private string _dlqUrl;

        private int _queueLengthWasZeroCount;
        private int _requeuedMessageCount;

        public void Requeue(string queueName, RequeuOptions options = default(RequeuOptions))
        {
            try
            {
                if (options == null)
                    options = new RequeuOptions();

                using (var client = GenerateClient(options))
                {
                    GetQueueUrls(client, queueName);

                    if (options.RequeueAll)
                    {
                        while (GetQueueLength(client) >= 0 && _queueLengthWasZeroCount < 3)
                        {
                            SendToQueueAndDeleteFromDLQ(client);
                        }
                    }
                    else
                    {
                        if (options.MessageIds != null && options.MessageIds.Count > 0)
                        {
                            while (GetQueueLength(client) >= 0 && _queueLengthWasZeroCount < 3 && _requeuedMessageCount < options.MessageIds.Count)
                            {
                                SendToQueueAndDeleteFromDLQ(client, options.MessageIds);
                            }
                        }
                        else
                        {
                            var count = GetQueueLength(client);

                            if (count < options.Count)
                                _logger.Info($"approximate number of messages in the {queueName}_deadletter is {count} which is lower than {options.Count}. Will requeue all messages in the queue");

                            while (GetQueueLength(client) >= 0 && _queueLengthWasZeroCount < 3 && _requeuedMessageCount < options.Count)
                            {
                                SendToQueueAndDeleteFromDLQ(client);
                            }
                        }
                    }

                    _logger.Info($"all messages requeued from {queueName}_deadletter to {queueName}");
                }
            }
            catch (System.Exception ex)
            {
                _logger.Error("exception occured", ex);
            }
            finally
            {
                _dlqUrl = string.Empty;
                _queueUrl = string.Empty;

                _queueLengthWasZeroCount = 0;
                _requeuedMessageCount = 0;
            }
            
        }

        private int GetQueueLength(AmazonSQSClient client)
        {
            var request = new GetQueueAttributesRequest(_dlqUrl, new List<string> { "ApproximateNumberOfMessages" });

            var response = client.GetQueueAttributes(request);

            var count = response.ApproximateNumberOfMessages;

            if (count == 0)
            {
                _queueLengthWasZeroCount++;
            }

            _logger.Info($"There are {count} messages in the dead letter queue");

            return count;

        }

        private AmazonSQSClient GenerateClient(RequeuOptions options)
        {
            if (!string.IsNullOrEmpty(options.ProfileName))
            {
                Amazon.Runtime.AWSCredentials credentials = new Amazon.Runtime.StoredProfileAWSCredentials(options.ProfileName);

                return new AmazonSQSClient(credentials);
            }

            return new AmazonSQSClient();
        }
        
        private void SendToQueueAndDeleteFromDLQ(AmazonSQSClient client, List<string> messageIds = null)
        {
            var receiveMessageRequest = new ReceiveMessageRequest();

            receiveMessageRequest.QueueUrl = _dlqUrl;
            receiveMessageRequest.MaxNumberOfMessages = 10;
            receiveMessageRequest.VisibilityTimeout = 10;

            var receiveMessageResponse = client.ReceiveMessage(receiveMessageRequest);

            if (receiveMessageResponse.HttpStatusCode == System.Net.HttpStatusCode.OK && receiveMessageResponse.Messages.Count > 0)
            {
                _logger.Info($"received {receiveMessageResponse.Messages.Count}");

                for (int i = 0; i < receiveMessageResponse.Messages.Count; i++)
                {
                    if (messageIds != null && !messageIds.Contains(receiveMessageResponse.Messages[i].MessageId))
                    {
                        _logger.Info($"message id {receiveMessageResponse.Messages[i].MessageId} is not in the defined message ids. Will skip.");

                        continue;
                    }

                    if (SendToQueue(client, _queueUrl, receiveMessageResponse.Messages[i].Body))
                    {
                        _logger.Info($"Message with id {receiveMessageResponse.Messages[i].MessageId} has been sent to the queue");

                        DeleteMessage(client, _dlqUrl, receiveMessageResponse.Messages[i].ReceiptHandle);

                        _logger.Info($"Message with id {receiveMessageResponse.Messages[i].MessageId} has been deleted from deadletter queue");

                        _requeuedMessageCount++;
                    }
                }
            }
        }

        private void GetQueueUrls(AmazonSQSClient client, string queueName)
        {
            _logger.Info($"Getting queue urls for the queue {queueName}");

            var request = new GetQueueUrlRequest(queueName);

            var response = client.GetQueueUrl(request);

            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                throw new System.Exception($"Cannot find the queue url for the queueName {request.QueueName}");

            _queueUrl = response.QueueUrl;
            _logger.Info($"queue urls for the queue {queueName} is {_queueUrl}");


            request.QueueName = $"{queueName}_deadletter";

            response = client.GetQueueUrl(request);

            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                throw new System.Exception($"Cannot find the queue url for the queueName {request.QueueName}");

            _dlqUrl = response.QueueUrl;
            _logger.Info($"queue urls for the queue {queueName}_deadletter is {_dlqUrl}");
        }

        private bool SendToQueue(AmazonSQSClient client, string queueUrl, string messageBody)
        {
            var request = new SendMessageRequest();
            request.QueueUrl = queueUrl;

            request.MessageBody = messageBody;

            var response = client.SendMessage(request);

            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        private bool DeleteMessage(AmazonSQSClient client, string queueUrl, string receiptHandle)
        {
            var request = new DeleteMessageRequest();
            request.QueueUrl = queueUrl;
            request.ReceiptHandle = receiptHandle;

            var response = client.DeleteMessage(request);

            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }
    }

    public class RequeuOptions
    {
        public RequeuOptions(int count = 0, params string[] messageIds)
        {
            Count = count;

            MessageIds = new List<string>(messageIds);

            if (count == 0 && (messageIds == null || messageIds.Length == 0))
                RequeueAll = true;
        }

        public int Count { get; private set; }

        public bool RequeueAll { get; private set; }

        public List<string> MessageIds { get; private set; }

        public string ProfileName { get; set; }
    }
}
