﻿using NUnit.Framework;
using Amazon.SQS;
using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using Tooling.SQS;

namespace Tooling.AWS.Tests
{
    [TestFixture]
    public class SQSToolingTest
    {
        private string _queueName;
        private string _dlqName;

        private string _queueUrl;
        private string _dlqueueUrl;

        [SetUp]
        public void SetUp()
        {
            CreateQueue();
        }

        [TearDown]
        public void TearDown()
        {
            DeleteQueue();
        }

        [TestCase(1)]
        [TestCase(15)]
        public void Requeue_WhenThereIsMessagesInTheDLQ_ShouldMoveThemToTheQueue(int count)
        {
            SendMessageToDLQ(count);

            var target = new SQSTooling();

            target.Requeue(_queueName);

            Thread.Sleep(2000);

            var messageCount = GetMessageCountOnQueue(_queueUrl);

            Assert.AreEqual(count, messageCount);

            var dlqMessageCount = GetMessageCountOnQueue(_dlqueueUrl);

            Assert.AreEqual(0, dlqMessageCount);
        }

        [TestCase(1, 1)]
        [TestCase(3, 1)]
        [TestCase(15, 4)]
        public void Requeue_WhenThereIsMessagesInTheDLQ_ShouldMoveTheGivenNumberOfMessagesToTheQueue(int count, int moveCount)
        {
            SendMessageToDLQ(count);

            var target = new SQSTooling();

            target.Requeue(_queueName, new RequeuOptions(moveCount));

            Thread.Sleep(2000);

            var messageCount = GetMessageCountOnQueue(_queueUrl);

            Assert.AreEqual(moveCount, messageCount);

            var dlqMessageCount = GetMessageCountOnQueue(_dlqueueUrl);

            Assert.AreEqual(count - moveCount, dlqMessageCount);
        }

        [Test]
        public void Requeue_WhenMessageIdsSpecified_ShouldOnlyMoveThoseMessageIds()
        {
            var messageIds = SendMessageToDLQ(3);

            var target = new SQSTooling();

            target.Requeue(_queueName, new RequeuOptions(0, messageIds[0]));

            Thread.Sleep(2000);

            var messageCount = GetMessageCountOnQueue(_queueUrl);

            Assert.AreEqual(1, messageCount);

            var dlqMessageCount = GetMessageCountOnQueue(_dlqueueUrl);

            Assert.AreEqual(2, dlqMessageCount);
        }

        private int GetMessageCountOnQueue(string queueUrl)
        {
            using (var client = new AmazonSQSClient())
            {
                var request = new GetQueueAttributesRequest(queueUrl, new System.Collections.Generic.List<string> { "ApproximateNumberOfMessages" });

                var response = client.GetQueueAttributes(request);

                return response.ApproximateNumberOfMessages;
            }
        }
        private List<string> SendMessageToDLQ(int count)
        {
            var messageIds = new List<string>();

            using(var client = new AmazonSQSClient())
            {
                for (int i = 0; i < count; i++)
                {
                    var request = new SendMessageRequest(_dlqueueUrl, $"Message Number {i}");
                    var response = client.SendMessage(request);

                    messageIds.Add(response.MessageId);
                }                
            }

            return messageIds;
        }

        private void CreateQueue()
        {
            using (var client = new AmazonSQSClient())
            {
                _queueName = $"tooling_test_{DateTime.Now.Ticks}";
                _dlqName = $"{_queueName}_deadletter";

                var request = new CreateQueueRequest(_queueName);

                var response = client.CreateQueue(request);

                if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception("Could not create queue");

                _queueUrl = response.QueueUrl;

                request.QueueName = _dlqName;

                response = client.CreateQueue(request);

                if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception("Could not create queue");

                _dlqueueUrl = response.QueueUrl;
            }
        }

        private void DeleteQueue()
        {
            using(var client = new AmazonSQSClient())
            {
                var request = new DeleteQueueRequest(_queueUrl);

                client.DeleteQueue(request);

                request.QueueUrl = _dlqueueUrl;

                client.DeleteQueue(request);
            }
        }
    }
}
