# README #

This repository contains set of tools to help operations issues on AWS.

## Requeueing messages from deadletter queue to queue ##

You need to run the tooling.aws.cmdline.exe with command parameters below

### Requeue all messages ###
`\tooling.aws.cmdline.exe -r <queueName>`

or,

`\tooling.aws.cmdline.exe --requeue <queueName>`

### Requeue some messages###
`\tooling.aws.cmdline.exe -r <queueName> <count>`

or,

`\tooling.aws.cmdline.exe --requeue <queueName> <count>`

### Requeue messages with message ids###
`\tooling.aws.cmdline.exe -r <queueName> 0 <MessageId1>,<MessageId2>...`

or,

`\tooling.aws.cmdline.exe --requeue <queueName> 0 <MessageId1>,<MessageId2>...`

### Requeue messages using a different aws profile ###
`\tooling.aws.cmdline.exe -r -p <profileName> <queueName>`

or,

`\tooling.aws.cmdline.exe --requeue --profile <profileName> <queueName>`

##TODO
+ Messages can be pulled async and requeued parallel
+ dead letter queue name can be specified
+ in the case of a failure of a message, there could be re-try logic instead of failing the whole command