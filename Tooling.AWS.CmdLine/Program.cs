﻿using System;
using CommandLine;
using CommandLine.Text;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Reflection;

namespace Tooling.AWS.CmdLine
{
    class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var options = new Options();
            var parser = new Parser();
            if (parser.ParseArguments(args, options))
            {
                if (options.Requeue)
                {
                    var stringBuilder = new StringBuilder();
                    stringBuilder.Append("Will requeue ");

                    var sqsTooling = new SQS.SQSTooling();

                    var queueName = options.Items[0];

                    int count = 0;
                    List<string> messageIds = new List<string>();

                    if (options.Items.Count > 1 && int.TryParse(options.Items[1], out count) && count > 0)
                        stringBuilder.Append($"{count} ");
                    else
                    {
                        stringBuilder.Append("all ");
                    }

                    stringBuilder.Append("messages ");

                    if (options.Items.Count > 2)
                    {
                        messageIds = options.Items[2].Split(',').Select(x => x.Trim()).ToList();

                        stringBuilder.Append($"with ids {options.Items[2]}");
                    }

                    _logger.Info($"{stringBuilder.ToString()} from {queueName}_deadletter to {queueName}");

                    sqsTooling.Requeue(queueName, new SQS.RequeuOptions(count, messageIds.ToArray()) { ProfileName = options.Profile });
                }
            }

            _logger.Info("Completed!");
        }
    }

    class Options
    {
        [Option('r', "requeue", HelpText = "Requeues the messages in deadletter queue to the target queue.")]
        public bool Requeue { get; set; }


        [Option('p', "profile", HelpText = "Define the profile name.")]
        public string Profile { get; set; }

        [ValueList(typeof(List<string>), MaximumElements = 100)]
        public IList<string> Items { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
